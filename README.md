---
title: Stable Diffusion Web UI
emoji: 🧿
colorFrom: blue
colorTo: blue
sdk: gradio
app_file: app.py
pinned: false
---

## Stable Diffusion Web UI
[https://github.com/AUTOMATIC1111/stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)

## Documentation
[https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki](https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki)

## Models License
https://huggingface.co/spaces/CompVis/stable-diffusion-license

## Demonstration
https://stablediffusion.fr/webui
https://stable-diffusion.net/webui
